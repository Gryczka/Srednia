/*
 * main.cpp
 *
 *  Created on: 13 lis 2016
 *      Author: Magdalena Gryczka
 */

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

class Przedmiot{
public:
	string nazwa;
	int ects;
	float ocena;
	Przedmiot();
	Przedmiot(string nazwaTemp, int ectsTemp, float ocenaTemp);
};

Przedmiot::Przedmiot() {
	nazwa=" ";
	ects=0;
	ocena=0;
}

Przedmiot::Przedmiot(string nazwaTemp, int ectsTemp, float ocenaTemp) {
	nazwa=nazwaTemp;
	ects=ectsTemp;
	ocena=ocenaTemp;
}

class Semestr{
public:
	vector <Przedmiot> przedmiot;
	string nazwaPlikuZPrzedmiotami;
	float srednia;
};

void odczytaniePliku(Semestr &Semestr, int numerSemestru) {
	fstream plikZPrzedmiotami;
	string wierszZPliku, numerSemestruString, temp;
	string nazwaPrzedmiotuTemp, ectsTempString;
	int ectsTemp;
	int dlugoscWiersza;

	numerSemestruString=to_string(numerSemestru);

	cout<<endl<<"SEMESTR "+numerSemestruString<<endl<<endl;

	plikZPrzedmiotami.open("semestr"+numerSemestruString);
	if(plikZPrzedmiotami.is_open()==1) {
		do {
			getline(plikZPrzedmiotami,wierszZPliku);
			if(!wierszZPliku.empty()) {
				nazwaPrzedmiotuTemp=wierszZPliku;
				ectsTempString=wierszZPliku;
				dlugoscWiersza=wierszZPliku.length();
				nazwaPrzedmiotuTemp=nazwaPrzedmiotuTemp.erase(dlugoscWiersza-1,1);
				ectsTempString=ectsTempString.erase(0,dlugoscWiersza-1);
				ectsTemp=atoi(ectsTempString.c_str());
				Semestr.przedmiot.push_back(Przedmiot(nazwaPrzedmiotuTemp,ectsTemp,2));
			}
		}while(plikZPrzedmiotami.good());
	}
	plikZPrzedmiotami.close();
}

void podanieOceny(Semestr &semestr) {
	for(int Iter=0; Iter<semestr.przedmiot.size(); Iter++) {
		cout<<semestr.przedmiot[Iter].nazwa<<" "<<semestr.przedmiot[Iter].ects<<endl;
		cin>>semestr.przedmiot[Iter].ocena;
	}
}

void wyswietalnieOcen(Semestr &semestr) {
	for(int Iter=0; Iter<semestr.przedmiot.size(); Iter++) {
		cout<<semestr.przedmiot[Iter].nazwa<<" "<<semestr.przedmiot[Iter].ects<<" "<<semestr.przedmiot[Iter].ocena
				<<endl;
	}
}

void liczenieSredniej(Semestr &semestr) {
	float temp=0;
	int dzielnik=0;

	for(int Iter=0; Iter<semestr.przedmiot.size(); Iter++) {
		temp+=semestr.przedmiot[Iter].ects*semestr.przedmiot[Iter].ocena;
		if(semestr.przedmiot[Iter].ocena==0)
			semestr.przedmiot[Iter].ects=0;
		dzielnik+=semestr.przedmiot[Iter].ects;
	}

	temp=temp/dzielnik;
	semestr.srednia=temp;
}

int main() {
	Semestr semestr[4];
	float srednia=0;

	for(int Iter=1; Iter<=4; Iter++) {
		odczytaniePliku(semestr[Iter-1],Iter);
		podanieOceny(semestr[Iter-1]);
		liczenieSredniej(semestr[Iter-1]);
		wyswietalnieOcen(semestr[Iter-1]);
		srednia+=semestr[Iter-1].srednia;
	}
	cout<<"Twoja srednia to "<<srednia/4<<endl;
}
